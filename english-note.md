<!--

author: Johnny Cubides

email: jgcubidesc@unal.edu.co

comment: 

version: 0.1.0

-->

# English notes

## Basic English Grammar

Pronouns - SHE, HER, HE, HIS
============================

!?[Pronouns - SHE, HER, HE, HIS](https://youtu.be/_IK_0sIsfxg)

[apuntes de sesion](./xournal/2022-04-08-Note-22-40.pdf)

## Phrases

* what do/did you say?
* i'd like coffe 
* i'm tired

## Tools

* App to translation [say hi](https://play.google.com/store/apps/details?id=com.sayhi.android.sayhitranslate)
* [API pronunciation](https://play.google.com/store/apps/details?id=com.aepronunciation.ipa)

* [Oxford 3000 abd 5000](https://www.oxfordlearnersdictionaries.com/wordlists/oxford3000-5000)

### Dictionary

* [The free dictionary](https://www.thefreedictionary.com/)
* [Cambridge dictionary](https://dictionary.cambridge.org/)
* [cambridge android](https://play.google.com/store/apps/details?id=com.aepronunciation.ipa)

### Pronunciation

* [American English Pronunciation Trainer](https://easypronunciation.com/en/practice-american-english-pronunciation-online)

## Grammar

* [Yes or no questions](https://englishstudypage.com/grammar/yesno-questions/)
